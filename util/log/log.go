package log

import (
	"encoding/json"
	"fmt"
	"os"
	"runtime"
	"strings"
	"time"
)

var startProcess time.Time
var requestID, logLevel string

type logger struct {
	RequestID string `json:"requestId"`         // The transaction id
	Level     string `json:"level"`             // The log level
	Func      string `json:"func"`              // The function name write log
	Msg       string `json:"msg,omitempty"`     // The log message
	Status    string `json:"status,omitempty"`  // The status log
	Elapsed   string `json:"elapsed,omitempty"` // The elapsed time at end of process (ms)
}

func Error(code, format string, args ...interface{}) {
	log := logger{}
	pc, _, lineno, ok := runtime.Caller(1)
	if ok {
		str := strings.Split(runtime.FuncForPC(pc).Name(), "/")
		log.Func = fmt.Sprintf("%s:%d", str[len(str)-1], lineno)
	}
	//log.Timestamp = time.Now().Format(time.RFC3339Nano)
	log.Level = "ERROR"
	log.RequestID = requestID
	log.Msg = fmt.Sprintf(format, args...)

	enc := json.NewEncoder(os.Stdout)
	enc.SetEscapeHTML(false)

	err := enc.Encode(log)
	if err != nil {
		fmt.Printf("Failed to encode log, %v\n", err)
	}
	fmt.Println("")

}

func Debug(format string, args ...interface{}) {
	if logLevel == "Debug" {
		log := logger{}
		pc, _, lineno, ok := runtime.Caller(1)
		if ok {
			str := strings.Split(runtime.FuncForPC(pc).Name(), "/")
			log.Func = fmt.Sprintf("%s:%d", str[len(str)-1], lineno)
		}
		//log.Timestamp = time.Now().Format(time.RFC3339Nano)
		//log.Suffix = config.GetString("kibana.suffix")
		log.Level = "DEBUG"
		log.RequestID = requestID
		log.Msg = fmt.Sprintf(format, args...)

		enc := json.NewEncoder(os.Stdout)
		enc.SetEscapeHTML(false)

		err := enc.Encode(log)
		if err != nil {
			fmt.Printf("Failed to encode log, %v\n", err)
		}
		fmt.Println("")

	}
}

func Info(format string, args ...interface{}) {
	log := logger{}
	pc, _, lineno, ok := runtime.Caller(1)
	if ok {
		str := strings.Split(runtime.FuncForPC(pc).Name(), "/")
		log.Func = fmt.Sprintf("%s:%d", str[len(str)-1], lineno)
	}
	//log.Timestamp = time.Now().Format(time.RFC3339Nano)
	log.Level = "INFO"
	log.RequestID = requestID

	if format == "START" {
		startProcess = time.Now()
		log.Msg = format
	}

	if format == "END" {
		log.Msg = format
		log.Status = args[0].(string)
		log.Elapsed = fmt.Sprintf("%d ms", getElapsedTime())
	} else {
		log.Msg = fmt.Sprintf(format, args...)
	}

	enc := json.NewEncoder(os.Stdout)
	enc.SetEscapeHTML(false)

	err := enc.Encode(log)
	if err != nil {
		fmt.Printf("Failed to encode log, %v\n", err)
	}
	fmt.Println("")

}

func SetLogLevel(level string) {
	logLevel = level
}

func SetRequestID(_requestID string) {
	requestID = _requestID
	Info("START")
}

func GetRequestID() string {
	return requestID
}

func getElapsedTime() int64 {
	return time.Now().Sub(startProcess).Nanoseconds() / int64(time.Millisecond)
}
