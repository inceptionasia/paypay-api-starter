package api

import (
	"encoding/json"
	"fmt"

	"src/util/log"

	"github.com/aws/aws-lambda-go/events"
)

type ResponseBody struct {
	Code    string `json:"code"`
	Message string `json:"message"`
	Detail  string `json:"detail,omitempty"`
}

func Response(status int, body ResponseBody) (*events.APIGatewayProxyResponse, error) {
	res := events.APIGatewayProxyResponse{
		Headers: map[string]string{"Content-Type": "application/json"},
	}
	res.StatusCode = status

	stringBody, _ := json.Marshal(body)
	res.Body = string(stringBody)

	if status == 200 {
		log.Info("END", "SUCCESS")
	} else {
		log.Error(body.Code, "%s", body.Detail)
		log.Info("END", "ERROR")
	}
	return &res, nil
}

func PrintRequest(req events.APIGatewayProxyRequest) {
	fmt.Println("Resource", req.Resource)
	fmt.Println("Path", req.Path)
	fmt.Println("HTTPMethod", req.HTTPMethod)
	fmt.Println("Headers", req.Headers)
	fmt.Println("MultiValueHeaders", req.MultiValueHeaders)
	fmt.Println("QueryStringParameters", req.QueryStringParameters)
	fmt.Println("MultiValueQueryStringParameters", req.MultiValueQueryStringParameters)
	fmt.Println("PathParameters", req.PathParameters)
	fmt.Println("StageVariables", req.StageVariables)
	fmt.Println("RequestContext", req.RequestContext)
	fmt.Println("Body", req.Body)
	fmt.Println("IsBase64Encoded", req.IsBase64Encoded)
}
