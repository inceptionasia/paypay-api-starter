package main

import (
	"context"
	"net/http"

	//Internal Lib
	"src/util/api"
	"src/util/log"

	//External Lib
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-lambda-go/lambdacontext"
)

func handler(ctx context.Context, req events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	lc, _ := lambdacontext.FromContext(ctx)
	log.SetRequestID(lc.AwsRequestID)

	// Route Mapping between Endpoint and Controller
	if req.Resource == "xxx" {
		//return ctrl.xxx
	}

	// Default Response for Route not match
	return api.Response(http.StatusNotFound, api.ResponseBody{
		Code:    "S001",
		Message: "Route not match",
		Detail:  "",
	})
}

func main() {
	lambda.Start(handler)
}
