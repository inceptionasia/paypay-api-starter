module src

go 1.17

require (
	github.com/aws/aws-lambda-go v1.26.0
	github.com/aws/aws-sdk-go-v2 v1.40.49
	github.com/google/uuid v1.3.0
	github.com/stretchr/testify v1.7.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)

require github.com/jmespath/go-jmespath v0.4.0 // indirect
